# Turtlebot Navigation based on RTAB-Map SLAM
## Packages Install on Ubuntu16.04
> 1.[RTAB-Map ROS Wrapper](http://wiki.ros.org/rtabmap_ros)
>> sudo apt-get install ros-kinetic-rtabmap ros-kinetic-rtabmap-ros

> 2.[ROS move-base](http://wiki.ros.org/move_base)

> 3.[Latest RealsenseD435i SDK](https://github.com/IntelRealSense/librealsense/releases/tag/v2.25.0)

>4.[RealsenseD435i ROS Wrapper](https://github.com/IntelRealSense/librealsense/releases/tag/v2.25.0)

## Compile rtabmap_realsense package
>1. cd ros_ws && catkin_init_workspace 
make sure a CMakeLists.txt is generated at current directory.

>2. catkin_make or catkin build 
There are actually no source code, just some launch files to integrate lots of nodes.

>3. source ./devel/setup.bash
You can add "source /path/to/workspace/devel/setup.bash" in ~/.bashrc file

## System startup steps
>1. turtlebot bringup and tf from "base_link" to "camera"(in the case that camera is not aligned with turtlebot baselink)
```sh
roslaunch rtabmap_realsense turtlebot_startup.launch
```
>2. d435i rgdb stratup
```sh
roslaunch rtabmap_realsense realsense_startup.launch
```
#### Mapping
>3. RTAB-Map startup and move_base
```sh
roslaunch rtabmap_realsense rtabmap_startup.launch
```
#### Localization
>3. RTAB-Map startup and move_base
```sh
roslaunch rtabmap_realsense rtabmap_startup.launch localization:=true
```
**Warning: The parameter "localization" is set to "false" default, if so, RTAB-Map will delete the map "rtabmpa.db" at ~/.ros directory.So make sure you have backup map file after mapping,and don't forget to set localization" to "true"**

##Parameters Tuning
* [navigation stack parameters tuning](http://wiki.ros.org/navigation/Tutorials/Navigation%20Tuning%20Guide)
Releated files have been put in rtabmap_realsense/param
* RTAB-Map parameters tuning
>1. I recomand [RTAB-Map paper](https://introlab.3it.usherbrooke.ca/mediawiki-introlab/images/7/7a/Labbe18JFR_preprint.pdf) to know the frrame of RTAB-Map
>2. [RTAB-Map ROS tutorials](http://wiki.ros.org/rtabmap_ros)